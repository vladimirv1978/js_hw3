let firstNumber = +prompt("Enter the first number, integer (m)", '');
let secondNumber = +prompt("Enter the second number, integer (n)", '');


while (!Number.isInteger(firstNumber) || !Number.isInteger(secondNumber) || firstNumber == 0 || secondNumber == 0 || firstNumber>=secondNumber) {
    alert("Not an integer or the first value is greater than the second");
    firstNumber = +prompt("Enter the first number, integer (m)", '');
    secondNumber = +prompt("Enter the second number, integer (n)", '');
}

for (let i=firstNumber; i<=secondNumber; i++) {
    let k=0;
    for (j=1; j<=i; j++) {
        if (i%j == 0) {
            k=k+1;
        }
    }
    if (k==2) {
        console.log("Prime number", i);
    }
}
