let anyNumber = +prompt("Please enter an integer", '');

while (!Number.isInteger(anyNumber) || anyNumber === 0) {
    anyNumber = +prompt("Not an integer. Please enter an integer", '');
}

if (anyNumber>=5) {
    for (let i=1; i <= anyNumber; i++) {
        if ( i%5 == 0) {
            console.log("Multiples of five - ", i);
        } 
    }
} else {
    alert("Sorry, no numbers");
}
